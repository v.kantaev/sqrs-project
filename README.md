# Flappy Bird Clone


[![pipeline status](https://gitlab.com/v.kantaev/test-sqrs-project-repo/badges/main/pipeline.svg)](https://gitlab.com/v.kantaev/test-sqrs-project-repo/-/commits/main)
[![coverage report](https://gitlab.com/v.kantaev/test-sqrs-project-repo/badges/main/coverage.svg)](https://gitlab.com/v.kantaev/test-sqrs-project-repo/-/commits/main)
[![Codacy Badge](https://app.codacy.com/project/badge/Grade/309fc8155cf74c0eb67fc3c01ab0fb01)](https://www.codacy.com/gl/v.kantaev/sqrs-project/dashboard?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=v.kantaev/sqrs-project&amp;utm_campaign=Badge_Grade)

> Team NoMoreWebPlease
 
> The project uses Unity 2020.3.16f1

## Project Structure

- `Core` - a C# solution with Simulation logic and unit tests.
- `Assets` - a Unity project with Presentation and View logic and integration tests.
 
## CI Pipeline

- Automatic formatting ([dotnet-format](https://github.com/dotnet/format))
- Naming tests (Unity EditMode tests) 
- Codacy integration: results are available [here](https://app.codacy.com/gl/v.kantaev/sqrs-project/dashboard).
- Unit tests
- Mutation testing ([stryker](https://github.com/stryker-mutator/stryker-net))
- Integration tests (Unity PlayMode tests)
- UI tests (Unity PlayMode tests)
- Performance tests (Unity PlayMode tests, via [Performance Testing API](https://docs.unity3d.com/Packages/com.unity.test-framework.performance@1.0/manual/index.html))
  - Frame Time
  - Memory
- WebGL build - access [here](https://v.kantaev.gitlab.io/sqrs-project/)
- Deployment to GitLab Pages
  - Only on push to `main`
