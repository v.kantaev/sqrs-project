﻿using Leopotam.Ecs;

namespace Core
{
    public class HelloWorldSystem : IEcsInitSystem
    {
        private readonly ILogger _logger;

        public HelloWorldSystem(ILogger logger) => _logger = logger;

        public void Init()
        {
            _logger.Log("Hello world!");
        }
    }
}