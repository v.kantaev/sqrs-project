using Core.Movement;
using Core.Public;
using Leopotam.Ecs;

namespace Core.Death
{
    public class DeathZoneSystem : IEcsRunSystem
    {
        private readonly IDeathZone _deathZone = default;
        private readonly RuntimeData _runtimeData = default;

        public void Run()
        {
            if (TryGetPlayerZ(out var z))
                _deathZone.SetZ(z);
        }

        private bool TryGetPlayerZ(out float z)
        {
            var playerEntity = _runtimeData.PlayerEntity;
            if (!playerEntity.IsAlive())
            {
                z = default;
                return false;
            }

            z = playerEntity.Get<PositionData>().Position.Z;
            return true;
        }
    }
}