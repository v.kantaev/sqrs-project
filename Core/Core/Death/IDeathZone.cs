namespace Core.Death
{
    public interface IDeathZone
    {
        void SetZ(float z);
    }
}