using Core.Bootstrap;
using Core.Movement;
using Leopotam.Ecs;

namespace Core.Death
{
    public class PlayerDeathSystem : IEcsRunSystem
    {
        private readonly EcsFilter<PlayerTag, DeathCommand> _filter = default;

        private readonly EcsWorld _world = default;

        public void Run()
        {
            foreach (var i in _filter)
            {
                var playerEntity = _filter.GetEntity(i);
                playerEntity.Get<VelocityData>().Velocity.Z = 0f;
                _world.NewEntity().Get<PlayerDeathEvent>();
            }
        }
    }
}