using Leopotam.Ecs;

namespace Core.Death
{
    public struct PlayerDeathEvent : IEcsIgnoreInFilter { }
}