using Core.Bootstrap;
using Core.Public;
using Leopotam.Ecs;

namespace Core.Death
{
    public class PlayerDeathFinalizeSystem : IEcsRunSystem
    {
        private readonly EcsFilter<PlayerDeathEvent> _deathEvents = default;
        private readonly EcsFilter<PlayerTag> _playerFilter = default;
        private readonly RuntimeData _runtimeData = default;

        public void Run()
        {
            foreach (var _ in _deathEvents)
            {
                foreach (var i in _playerFilter)
                {
                    _playerFilter.GetEntity(i)
                        .Destroy();
                }

                _runtimeData.PlayerEntity = default;
                break;
            }
        }
    }
}