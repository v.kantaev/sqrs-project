using System.Numerics;
using Core.Movement;
using Core.Public;
using Leopotam.Ecs;

namespace Core.Bootstrap
{
    public class PlayerSpawnSystem : IEcsInitSystem
    {
        private readonly IPlayerViewFactory _playerViewFactory = default;
        private readonly RuntimeData _runtimeData = default;
        private readonly ISceneData _sceneData = default;
        private readonly IStaticData _staticData = default;

        public void Init()
        {
            var playerEntity = _playerViewFactory.Create(_sceneData.PlayerSpawnPosition, Quaternion.Identity);
            playerEntity.Get<PlayerTag>();
            playerEntity.Get<VelocityData>().Velocity = new Vector3(0, 0, _staticData.PlayerHorizontalSpeed);
            playerEntity.Get<AccelerationData>().Acceleration = new Vector3(0, -_staticData.Gravity, 0);
            _runtimeData.PlayerEntity = playerEntity;
        }
    }
}