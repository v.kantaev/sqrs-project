using System.Numerics;

namespace Core.Movement
{
    public struct AccelerationData
    {
        public Vector3 Acceleration;
    }
}