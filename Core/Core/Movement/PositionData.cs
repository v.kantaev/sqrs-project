using System.Numerics;

namespace Core.Movement
{
    public struct PositionData
    {
        public Vector3 Position;
    }
}