using Core.Bootstrap;
using Core.Public;
using Leopotam.Ecs;

namespace Core.Movement
{
    public class PlayerJumpInputSystem : IEcsRunSystem
    {
        private readonly IControls _controls = default;
        private readonly EcsFilter<PlayerTag, AllowInputTag> _filter = default;

        public void Run()
        {
            foreach (var i in _filter)
            {
                if (_controls.JumpPressed)
                    _filter.GetEntity(i).Get<JumpCommand>();
            }
        }
    }
}