using Core.Public;
using Leopotam.Ecs;

namespace Core.Movement
{
    public class JumpSystem : IEcsRunSystem
    {
        private readonly EcsFilter<VelocityData, JumpCommand> _filter = default;
        private readonly IStaticData _staticData = default;

        public void Run()
        {
            foreach (var i in _filter)
            {
                ref var velocity = ref _filter.Get1(i).Velocity;
                if (velocity.Y < 0f)
                    velocity.Y = _staticData.JumpSpeed;
                else
                    velocity.Y += _staticData.JumpSpeed;
            }
        }
    }
}