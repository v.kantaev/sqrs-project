using Core.Public;
using Leopotam.Ecs;

namespace Core.Movement
{
    public class VelocitySystem : IEcsRunSystem
    {
        private readonly RuntimeData _data = default;
        private readonly EcsFilter<VelocityData, PositionData> _filter = default;

        public void Run()
        {
            var dt = _data.Dt;
            foreach (var i in _filter)
            {
                ref var positionData = ref _filter.Get2(i);
                positionData.Position += _filter.Get1(i).Velocity * dt;
            }
        }
    }
}