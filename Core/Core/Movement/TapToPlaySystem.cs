using Core.Public;
using Leopotam.Ecs;

namespace Core.Movement
{
    public class TapToPlaySystem : IEcsRunSystem
    {
        private readonly EcsFilter<TapToPlayEvent> _filter = default;
        private readonly RuntimeData _runtimeData = default;

        public void Run()
        {
            foreach (var i in _filter)
            {
                _runtimeData.PlayerEntity.Get<AllowInputTag>();
            }
        }
    }
}