using System.Numerics;

namespace Core.Movement
{
    public struct VelocityData
    {
        public Vector3 Velocity;
    }
}