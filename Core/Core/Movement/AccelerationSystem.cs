using Core.Public;
using Leopotam.Ecs;

namespace Core.Movement
{
    public class AccelerationSystem : IEcsRunSystem
    {
        private readonly EcsFilter<AccelerationData, VelocityData> _filter = default;
        private readonly RuntimeData _runtimeData = default;

        public void Run()
        {
            var dt = _runtimeData.Dt;
            foreach (var i in _filter)
            {
                var acceleration = _filter.Get1(i).Acceleration;
                _filter.Get2(i).Velocity += acceleration * dt;
            }
        }
    }
}