﻿using Core.Public;
using Leopotam.Ecs;

namespace Core.Bonuses
{
    public class BonusInvincibilityTimerSystem : IEcsRunSystem
    {
        private readonly EcsFilter<BonusInvincibilityData> _filter = default;
        private readonly RuntimeData _runtimeData = default;
        private readonly IStaticData _staticData = default;

        public void Run()
        {
            foreach (var i in _filter)
            {
                ref var elapsedTime = ref _filter.Get1(i).ElapsedTime;
                elapsedTime += _runtimeData.Dt;
                if (elapsedTime >= _staticData.InvincibilityTime)
                    _filter.GetEntity(i).Del<BonusInvincibilityData>();
            }
        }
    }
}