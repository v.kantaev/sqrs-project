﻿namespace Core.Bonuses
{
    public struct BonusInvincibilityData
    {
        public float ElapsedTime;
    }
}