using System.Numerics;
using Core.Skins;

namespace Core.Public
{
    public interface ISceneData
    {
        Vector3 PlayerSpawnPosition { get; }
        ISkinButtonManager SkinButtonManager { get; }
    }
}