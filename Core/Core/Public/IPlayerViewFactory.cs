using System.Numerics;
using Leopotam.Ecs;

namespace Core.Public
{
    public interface IPlayerViewFactory
    {
        EcsEntity Create(Vector3 position, Quaternion rotation);
    }
}