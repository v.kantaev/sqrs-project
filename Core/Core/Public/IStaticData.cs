using Core.LevelGeneration;

namespace Core.Public
{
    public interface IStaticData
    {
        float PlayerHorizontalSpeed { get; }
        float JumpSpeed { get; }
        float Gravity { get; }
        float InvincibilityTime { get; }
        LevelGenerationParams LevelGenerationParams { get; }
    }
}