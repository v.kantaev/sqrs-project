using Leopotam.Ecs;

namespace Core.Public
{
    public class RuntimeData
    {
        public float Dt;
        public EcsEntity PlayerEntity;
    }
}