using Core.Death;
using Leopotam.Ecs;

namespace Core.Score
{
    public class PublishScoreSystem : IEcsRunSystem
    {
        private readonly EcsFilter<PlayerDeathEvent> _filter = default;
        private readonly IScoreboardService _scoreboardService = default;
        private readonly ScoreModel _scoreModel = default;

        public void Run()
        {
            if (_filter.GetEntitiesCount() > 0)
                _scoreboardService.SendScore(_scoreModel.Score);
        }
    }
}