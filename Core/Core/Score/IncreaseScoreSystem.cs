using Leopotam.Ecs;

namespace Core.Score
{
    public class IncreaseScoreSystem : IEcsRunSystem
    {
        private readonly EcsFilter<IncreaseScoreCommand> _filter = default;
        private readonly ScoreModel _scoreModel = default;

        public void Run()
        {
            foreach (var _ in _filter)
            {
                _scoreModel.AddScore(1);
            }
        }
    }
}