namespace Core.Score
{
    public class ScoreboardEntry
    {
        public int Position;
        public int Score;
        public string UserId;
    }
}