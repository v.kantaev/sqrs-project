using System;
using System.Collections.Generic;

namespace Core.Score
{
    public interface IScoreboardService
    {
        void SendScore(int score);
        void GetLeaderboard(int maxResults, Action<IEnumerable<ScoreboardEntry>> onSuccess, Action<string> onError);
        string GetUserId();
    }
}