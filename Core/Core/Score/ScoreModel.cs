using System;

namespace Core.Score
{
    public class ScoreModel
    {
        public int Score { get; private set; }

        public void AddScore(int score)
        {
            Score += score;
            Changed?.Invoke();
        }

        public event Action Changed;
    }
}