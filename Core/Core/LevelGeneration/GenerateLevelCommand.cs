using System.Numerics;

namespace Core.LevelGeneration
{
    public struct GenerateLevelCommand
    {
        public Vector3 LevelOrigin;
        public LevelGenerationParams Params;
    }
}