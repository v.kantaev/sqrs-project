using System.Numerics;

namespace Core.LevelGeneration
{
    public struct CreatePipeCommand
    {
        public Vector3 Position;
        public Quaternion Rotation;
        public LevelGenerationParams Params;
    }
}