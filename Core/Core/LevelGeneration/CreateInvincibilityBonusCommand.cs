using System.Numerics;

namespace Core.LevelGeneration
{
    public struct CreateInvincibilityBonusCommand
    {
        public Vector3 Position;
    }
}