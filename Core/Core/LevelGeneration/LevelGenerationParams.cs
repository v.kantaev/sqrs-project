using System;

namespace Core.LevelGeneration
{
    [Serializable]
    public struct LevelGenerationParams
    {
        public Range StepRange;
        public Range YOffsetRange;
        public int Count;
        public float InitialStep;
        public int MaxPipesWithSameSignInARow;
        public float BonusProbability;
    }
}