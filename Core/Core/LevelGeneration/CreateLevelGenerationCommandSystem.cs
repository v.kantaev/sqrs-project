using Core.Public;
using Leopotam.Ecs;

namespace Core.LevelGeneration
{
    public class CreateLevelGenerationCommandSystem : IEcsInitSystem
    {
        private readonly ISceneData _sceneData = default;
        private readonly IStaticData _staticData = default;
        private readonly EcsWorld _world = default;

        public void Init()
        {
            ref var generateLevelCommand = ref _world.NewEntity().Get<GenerateLevelCommand>();
            generateLevelCommand.LevelOrigin = _sceneData.PlayerSpawnPosition;
            generateLevelCommand.Params = _staticData.LevelGenerationParams;
        }
    }
}