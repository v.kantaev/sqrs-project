using System;
using System.Numerics;
using Leopotam.Ecs;

namespace Core.LevelGeneration
{
    public class GenerateLevelSystem : IEcsRunSystem
    {
        private readonly EcsFilter<GenerateLevelCommand> _filter = default;
        private readonly Random _random = new Random();
        private readonly EcsWorld _world = default;

        public void Run()
        {
            foreach (var i in _filter)
            {
                var generateLevelCommand = _filter.Get1(i);
                var @params = generateLevelCommand.Params;
                var pipesWithSameSign = 0;
                var lastSign = 0;

                var origin = generateLevelCommand.LevelOrigin;
                origin.Z += @params.InitialStep;

                for (var pipeIndex = 0; pipeIndex < @params.Count; pipeIndex++)
                {
                    var entity = _world.NewEntity();
                    ref var createPipeCommand = ref entity.Get<CreatePipeCommand>();

                    var pipePosition = origin;
                    var sign = _random.Next(2) == 0 ? 1 : -1;
                    if (lastSign == sign)
                    {
                        pipesWithSameSign++;
                        if (pipesWithSameSign > @params.MaxPipesWithSameSignInARow)
                        {
                            sign = -sign;
                            pipesWithSameSign = 1;
                        }
                    }
                    else
                    {
                        pipesWithSameSign = 1;
                    }

                    lastSign = sign;
                    var offset = Lerp(@params.YOffsetRange.Min, @params.YOffsetRange.Max, (float) _random.NextDouble());
                    pipePosition.Y += sign * offset;

                    createPipeCommand.Position = pipePosition;
                    createPipeCommand.Rotation = sign > 0
                        ? Quaternion.CreateFromAxisAngle(new Vector3(1, 0, 0), DegToRad(180))
                        : Quaternion.Identity;

                    origin.Z += Lerp(@params.StepRange.Min, @params.StepRange.Max, (float) _random.NextDouble());

                    if (_random.NextDouble() > @params.BonusProbability) continue;

                    var bonusPosition = pipePosition;
                    bonusPosition.Y = origin.Y;
                    _world.NewEntity().Get<CreateInvincibilityBonusCommand>().Position = bonusPosition;
                }
            }
        }

        private static float DegToRad(float angle) => (float) (Math.PI * angle / 180.0);

        private static float Lerp(float a, float b, float t)
        {
            t = Clamp(t, 0, 1);
            return a + (b - a) * t;
        }

        private static float Clamp(float value, float min, float max) => Math.Max(min, Math.Min(max, value));
    }
}