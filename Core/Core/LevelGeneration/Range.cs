using System;

namespace Core.LevelGeneration
{
    [Serializable]
    public struct Range
    {
        public float Min;
        public float Max;
    }
}