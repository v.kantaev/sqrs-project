using Core.Public;
using Leopotam.Ecs;

namespace Core.Skins
{
    public class SkinSystem : IEcsRunSystem, IEcsInitSystem
    {
        private readonly EcsFilter<SetSkinCommand> _filter = default;
        private readonly IPlayerSkinView _playerSkinView = default;
        private readonly ISceneData _sceneData = default;
        private readonly IPlayerSkinModel _skinModel = default;
        private readonly EcsWorld _world = default;

        public void Init()
        {
            _sceneData.SkinButtonManager.Init(_skinModel, _world);
            _world.NewEntity().Get<SetSkinCommand>().SkinIndex = _skinModel.SkinIndex;
        }


        public void Run()
        {
            if (!_playerSkinView.CanSetSkin()) return;

            foreach (var i in _filter)
            {
                var setSkinCommand = _filter.Get1(i);
                _playerSkinView.SetSkin(setSkinCommand.SkinIndex);
                _skinModel.SkinIndex = setSkinCommand.SkinIndex;
            }
        }
    }
}