namespace Core.Skins
{
    public interface IPlayerSkinView
    {
        bool CanSetSkin();
        void SetSkin(int skinIndex);
        void Hide();
        void Show();
    }
}