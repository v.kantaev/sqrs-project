namespace Core.Skins
{
    public interface IPlayerSkinModel
    {
        int SkinIndex { get; set; }
        int SkinsCount { get; }
    }
}