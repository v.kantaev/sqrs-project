using Leopotam.Ecs;

namespace Core.Skins
{
    public interface ISkinButtonManager
    {
        void Init(IPlayerSkinModel playerSkinModel, EcsWorld world);
    }
}