using Core.Public;
using Core.Skins;
using Leopotam.Ecs;
using NSubstitute;
using NUnit.Framework;

namespace Core.Tests.Skins;

public class SkinSystemTests
{
    [Test]
    [TestCase(0)]
    [TestCase(1)]
    [TestCase(2)]
    public void GivenSkinSystem_WhenFirstRun_ThenSkinViewUpdates(int skinIndex)
    {
        // Arrange
        var world = new EcsWorld();
        var ecsSystems = new EcsSystems(world);
        ecsSystems.Add(new SkinSystem());

        var playerSkinModel = Substitute.For<IPlayerSkinModel>();
        playerSkinModel.SkinIndex.Returns(skinIndex);

        var playerSkinView = Substitute.For<IPlayerSkinView>();
        playerSkinView.CanSetSkin().Returns(true);

        var sceneData = Substitute.For<ISceneData>();
        sceneData.SkinButtonManager.Returns(Substitute.For<ISkinButtonManager>());
        ecsSystems
            .Inject(playerSkinView)
            .Inject(sceneData)
            .Inject(playerSkinModel)
            ;
        ecsSystems.Init();

        // Act
        ecsSystems.Run();

        // Assert
        playerSkinView.Received().SetSkin(skinIndex);
    }
}