using Core.Bonuses;
using Core.Public;
using FluentAssertions;
using Leopotam.Ecs;
using NSubstitute;
using NUnit.Framework;

namespace Core.Tests.Bonuses;

public class BonusInvincibilityTimerSystemTests
{
    private const float InvincibilityTime = 1f;
    private EcsSystems _ecsSystems;
    private RuntimeData _runtimeData;
    private EcsWorld _world;

    [SetUp]
    public void SetUp()
    {
        _world = new EcsWorld();
        _ecsSystems = new EcsSystems(_world);
        _ecsSystems.Add(new BonusInvincibilityTimerSystem());
        _runtimeData = new RuntimeData();

        var staticData = Substitute.For<IStaticData>();
        staticData.InvincibilityTime.Returns(InvincibilityTime);

        _ecsSystems.Inject(_runtimeData);
        _ecsSystems.Inject(staticData);
        _ecsSystems.ProcessInjects();
        _ecsSystems.Init();
    }

    [Test]
    public void GivenElapsedTimeBelowThreshold_WhenRun_ThenComponentRemains()
    {
        // Arrange
        var entity = _world.NewEntity();
        entity.Get<int>();
        ref var bonusInvincibilityData = ref entity.Get<BonusInvincibilityData>();
        bonusInvincibilityData.ElapsedTime = 0f;
        _runtimeData.Dt = InvincibilityTime * 0.5f;

        // Act
        _ecsSystems.Run();

        // Assert
        entity.Has<BonusInvincibilityData>().Should().BeTrue();
    }

    [Test]
    public void GivenElapsedTimeAboveThreshold_WhenRun_ThenComponentGetRemoved()
    {
        // Arrange
        var entity = _world.NewEntity();
        entity.Get<int>();
        ref var bonusInvincibilityData = ref entity.Get<BonusInvincibilityData>();
        bonusInvincibilityData.ElapsedTime = 0f;
        _runtimeData.Dt = InvincibilityTime * 1.5f;

        // Act
        _ecsSystems.Run();

        // Assert
        entity.Has<BonusInvincibilityData>().Should().BeFalse();
    }
}