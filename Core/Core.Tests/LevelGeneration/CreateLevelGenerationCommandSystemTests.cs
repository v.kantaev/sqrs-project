using Core.LevelGeneration;
using Core.Public;
using FluentAssertions;
using Leopotam.Ecs;
using NSubstitute;
using NUnit.Framework;

namespace Core.Tests.LevelGeneration;

public class CreateLevelGenerationCommandSystemTests
{
    private ISceneData _sceneData;
    private IStaticData _staticData;
    private EcsSystems _systems;
    private EcsWorld _world;

    [SetUp]
    public void SetUp()
    {
        _world = new EcsWorld();
        _systems = new EcsSystems(_world);

        _sceneData = Substitute.For<ISceneData>();
        _staticData = Substitute.For<IStaticData>();
    }

    [Test]
    public void GivenInit_WhenCheckingCommands_ThenThereIsOne()
    {
        // Arrange
        _systems.Add(new CreateLevelGenerationCommandSystem())
            .Inject(_sceneData)
            .Inject(_staticData);

        // Act
        _systems.Init();

        // Assert
        var commandsFilter = _world.GetFilter(typeof(EcsFilter<GenerateLevelCommand>));
        commandsFilter.GetEntitiesCount().Should().Be(1);
    }
}