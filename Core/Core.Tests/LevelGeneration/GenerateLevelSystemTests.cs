using Core.LevelGeneration;
using FluentAssertions;
using Leopotam.Ecs;
using NUnit.Framework;

namespace Core.Tests.LevelGeneration;

[TestFixture]
public class GenerateLevelSystemTests
{
    [SetUp]
    public void SetUp()
    {
        _world = new EcsWorld();
        _systems = new EcsSystems(_world);
    }

    private EcsSystems _systems;
    private EcsWorld _world;

    [Test]
    [TestCase(0)]
    [TestCase(1)]
    [TestCase(5)]
    public void GivenRun_WhenThereIsCommand_ThenThereArePipeCommands(int pipesCount)
    {
        // Arrange
        _world.NewEntity().Get<GenerateLevelCommand>().Params.Count = pipesCount;
        _systems.Add(new GenerateLevelSystem())
            .Init();

        // Act
        _systems.Run();

        // Assert
        var pipeCommandsFilter = _world.GetFilter(typeof(EcsFilter<CreatePipeCommand>));
        pipeCommandsFilter.GetEntitiesCount().Should().Be(pipesCount);
    }

    [Test]
    public void GivenRun_WhenThereAreNoCommands_ThenThereNoPipeCommands()
    {
        // Arrange
        _systems.Add(new GenerateLevelSystem())
            .Init();

        // Act
        _systems.Run();

        // Assert
        var pipeCommandsFilter = _world.GetFilter(typeof(EcsFilter<CreatePipeCommand>));
        pipeCommandsFilter.GetEntitiesCount().Should().Be(0);
    }

    [Test]
    public void GivenRun_WhenInitialStepIsAboveZero_ThenPipeZPositionsAreAboveZero()
    {
        // Arrange
        ref var levelGenerationParams = ref _world.NewEntity().Get<GenerateLevelCommand>().Params;
        levelGenerationParams.Count = 1;
        levelGenerationParams.InitialStep = 1;

        _systems.Add(new GenerateLevelSystem())
            .Init();

        // Act
        _systems.Run();

        // Assert
        var pipeCommandsFilter = (EcsFilter<CreatePipeCommand>) _world.GetFilter(typeof(EcsFilter<CreatePipeCommand>));

        foreach (var i in pipeCommandsFilter)
        {
            pipeCommandsFilter.Get1(i).Position.Z.Should().BeGreaterThan(0);
        }
    }
}