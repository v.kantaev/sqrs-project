using System.Numerics;
using Core.Bootstrap;
using Core.Movement;
using Core.Public;
using FluentAssertions;
using Leopotam.Ecs;
using NSubstitute;
using NUnit.Framework;

namespace Core.Tests.Bootstrap;

public class PlayerSpawnSystemTests
{
    private IPlayerViewFactory _playerViewFactory;
    private RuntimeData _runtimeData;
    private IStaticData _staticData;
    private EcsSystems _systems;
    private EcsWorld _world;

    [SetUp]
    public void SetUp()
    {
        _world = new EcsWorld();
        _systems = new EcsSystems(_world);
        _runtimeData = new RuntimeData();

        _playerViewFactory = Substitute.For<IPlayerViewFactory>();
        var newEntity = _world.NewEntity();
        newEntity.Get<int>();
        _playerViewFactory.Create(Arg.Any<Vector3>(), Arg.Any<Quaternion>())
            .Returns(newEntity);

        _staticData = Substitute.For<IStaticData>();
        _systems.Add(new PlayerSpawnSystem())
            .Inject(Substitute.For<ISceneData>())
            .Inject(_staticData)
            .Inject(_runtimeData)
            .Inject(_playerViewFactory)
            ;
    }

    [Test]
    public void GivenPlayerSpawnSystem_WhenInit_ThenPlayerIsSpawned()
    {
        // Arrange

        // Act
        _systems.Init();

        // Assert
        _playerViewFactory.Received().Create(Arg.Any<Vector3>(), Arg.Any<Quaternion>());
        _runtimeData.PlayerEntity.IsAlive().Should().BeTrue();
    }

    [Test]
    public void GivenPlayerSpawnSystem_WhenInit_ThenThereIsPlayerEntity()
    {
        // Arrange

        // Act
        _systems.Init();

        // Assert
        var filter = _world.GetFilter(typeof(EcsFilter<PlayerTag>));
        filter.GetEntitiesCount().Should().Be(1);
    }

    [Test]
    public void GivenPlayerSpawnSystem_WhenInit_ThenAccelerationIsNegative()
    {
        // Arrange
        _staticData.Gravity.Returns(5);

        // Act
        _systems.Init();

        // Assert
        var filter =
            (EcsFilter<PlayerTag, AccelerationData>) _world.GetFilter(typeof(EcsFilter<PlayerTag, AccelerationData>));
        foreach (var i in filter)
        {
            filter.Get2(i).Acceleration.Y.Should().BeNegative();
        }
    }
}