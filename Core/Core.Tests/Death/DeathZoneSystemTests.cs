using System.Numerics;
using Core.Death;
using Core.Movement;
using Core.Public;
using Leopotam.Ecs;
using NSubstitute;
using NUnit.Framework;

namespace Core.Tests.Death;

public class DeathZoneSystemTests
{
    private IDeathZone _deathZone;
    private EcsSystems _ecsSystems;
    private RuntimeData _runtimeData;
    private EcsWorld _world;

    [SetUp]
    public void SetUp()
    {
        _world = new EcsWorld();
        _ecsSystems = new EcsSystems(_world);
        _deathZone = Substitute.For<IDeathZone>();
        _runtimeData = new RuntimeData();
        _ecsSystems.Add(new DeathZoneSystem());

        _ecsSystems
            .Inject(_deathZone)
            .Inject(_runtimeData)
            ;
    }

    [Test]
    public void GivenDeathZoneSystem_WhenThereIsNotPlayer_ThenDoNothing()
    {
        // Arrange
        _ecsSystems.Init();

        // Act
        _ecsSystems.Run();

        // Assert
        _deathZone.DidNotReceive().SetZ(Arg.Any<float>());
    }

    [Test]
    public void GivenDeathZoneSystem_WhenThereIsDeadPlayer_ThenDoNothing()
    {
        // Arrange
        _ecsSystems.Init();
        var entity = _world.NewEntity();
        _runtimeData.PlayerEntity = entity;
        entity.Destroy();

        // Act
        _ecsSystems.Run();

        // Assert
        _deathZone.DidNotReceive().SetZ(Arg.Any<float>());
    }

    [Test]
    public void GivenDeathZoneSystem_WhenThereIsAlivePlayer_ThenSetZ()
    {
        // Arrange
        _ecsSystems.Init();
        var entity = _world.NewEntity();
        const float positionZ = 10f;
        entity.Get<PositionData>().Position = new Vector3(0, 0, positionZ);
        _runtimeData.PlayerEntity = entity;

        // Act
        _ecsSystems.Run();

        // Assert
        _deathZone.Received().SetZ(positionZ);
    }
}