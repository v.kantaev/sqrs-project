using NSubstitute;
using NUnit.Framework;

namespace Core.Tests
{
    public class HelloWorldSystemTests
    {
        [Test]
        public void InitTest()
        {
            var logger = Substitute.For<ILogger>();
            var helloWorldSystem = new HelloWorldSystem(logger);

            helloWorldSystem.Init();

            logger.Received().Log("Hello world!");
        }
    }
}