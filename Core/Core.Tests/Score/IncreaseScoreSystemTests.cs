using Core.Score;
using FluentAssertions;
using Leopotam.Ecs;
using NUnit.Framework;

namespace Core.Tests.Score;

public class IncreaseScoreSystemTests
{
    private EcsSystems _ecsSystems;
    private ScoreModel _scoreModel;
    private EcsWorld _world;

    [SetUp]
    public void SetUp()
    {
        _world = new EcsWorld();
        _ecsSystems = new EcsSystems(_world);

        _scoreModel = new ScoreModel();

        _ecsSystems
            .Add(new IncreaseScoreSystem())
            .Inject(_scoreModel)
            .ProcessInjects()
            .Init();
    }

    [Test]
    [TestCase(0)]
    [TestCase(1)]
    [TestCase(5)]
    public void GivenNoCommands_WhenRun_ThenScoreStaysTheSame(int initialScore)
    {
        // Arrange
        _scoreModel.AddScore(initialScore);

        // Act
        _ecsSystems.Run();

        // Assert
        _scoreModel.Score.Should().Be(initialScore);
    }

    [Test]
    [TestCase(0, 1)]
    [TestCase(1, 1)]
    [TestCase(1, 5)]
    [TestCase(5, 5)]
    public void GivenCommands_WhenRun_ThenScoreIncreases(int initialScore, int commands)
    {
        // Arrange
        _scoreModel.AddScore(initialScore);

        for (var i = 0; i < commands; i++)
        {
            _world.NewEntity().Get<IncreaseScoreCommand>();
        }

        // Act
        _ecsSystems.Run();

        // Assert
        _scoreModel.Score.Should().Be(initialScore + commands);
    }
}