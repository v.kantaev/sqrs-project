using Core.Death;
using Core.Score;
using Leopotam.Ecs;
using NSubstitute;
using NUnit.Framework;

namespace Core.Tests.Score;

public class PublishScoreSystemTests
{
    private EcsSystems _ecsSystems;
    private IScoreboardService _scoreboardService;
    private ScoreModel _scoreModel;
    private EcsWorld _world;

    [SetUp]
    public void SetUp()
    {
        _world = new EcsWorld();
        _ecsSystems = new EcsSystems(_world);

        _scoreboardService = Substitute.For<IScoreboardService>();
        _scoreModel = new ScoreModel();

        _ecsSystems
            .Add(new PublishScoreSystem())
            .Inject(_scoreModel)
            .Inject(_scoreboardService)
            .ProcessInjects()
            .Init();
    }

    [Test]
    public void GivenNoDeathEvents_WhenRun_ThenNoScoreIsSent()
    {
        // Arrange

        // Act
        _ecsSystems.Run();

        // Assert
        _scoreboardService.DidNotReceive().SendScore(Arg.Any<int>());
    }

    [Test]
    [TestCase(1)]
    [TestCase(2)]
    [TestCase(5)]
    public void GivenDeathEvents_WhenRun_ThenScoreIsSentOnce(int deathEvents)
    {
        // Arrange
        _scoreModel.AddScore(3);

        for (var i = 0; i < deathEvents; i++)
        {
            _world.NewEntity().Get<PlayerDeathEvent>();
        }

        // Act
        _ecsSystems.Run();

        // Assert
        _scoreboardService.Received(1).SendScore(_scoreModel.Score);
    }
}