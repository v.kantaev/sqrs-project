using System.Numerics;
using Core.Movement;
using Core.Public;
using FluentAssertions;
using Leopotam.Ecs;
using NUnit.Framework;

namespace Core.Tests.Movement;

public class AccelerationSystemTests
{
    [Test]
    public void GivenAccelerationSystem_WhenRun_ThenVelocityIncreases()
    {
        // Arrange
        var world = new EcsWorld();
        var systems = new EcsSystems(world);
        var entity = world.NewEntity();
        entity.Get<VelocityData>();
        entity.Get<AccelerationData>().Acceleration = new Vector3(0f, 0f, 1f);
        systems.Add(new AccelerationSystem())
            .Inject(new RuntimeData { Dt = 1f })
            .Init();

        // Act
        systems.Run();

        // Assert
        entity.Get<VelocityData>()
            .Velocity
            .Length()
            .Should().BeApproximately(1f, 0.01f);
    }
}