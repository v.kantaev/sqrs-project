using System.Numerics;
using Core.Movement;
using Core.Public;
using FluentAssertions;
using Leopotam.Ecs;
using NUnit.Framework;

namespace Core.Tests.Movement;

public class VelocitySystemTests
{
    [Test]
    public void GivenVelocitySystem_WhenRun_ThenPositionIncreases()
    {
        // Arrange
        var world = new EcsWorld();
        var systems = new EcsSystems(world);
        var entity = world.NewEntity();
        entity.Get<PositionData>();
        entity.Get<VelocityData>().Velocity = new Vector3(0f, 0f, 1f);
        systems.Add(new VelocitySystem())
            .Inject(new RuntimeData { Dt = 1f })
            .Init();

        // Act
        systems.Run();

        // Assert
        entity.Get<PositionData>()
            .Position
            .Length()
            .Should().BeApproximately(1f, 0.01f);
    }
}