using Core.Bootstrap;
using Core.Movement;
using Core.Public;
using FluentAssertions;
using Leopotam.Ecs;
using NSubstitute;
using NUnit.Framework;

namespace Core.Tests.Movement;

public class PlayerJumpInputSystemTests
{
    private IControls _controls;
    private EcsSystems _systems;
    private EcsWorld _world;

    [SetUp]
    public void SetUp()
    {
        _world = new EcsWorld();
        _systems = new EcsSystems(_world);

        _controls = Substitute.For<IControls>();
    }

    [Test]
    public void GivenRun_WhenPressingJump_ThenAddsJumpCommand()
    {
        // Arrange
        var entity = _world.NewEntity();
        entity.Get<PlayerTag>();
        entity.Get<AllowInputTag>();
        _controls.JumpPressed.Returns(true);
        _systems.Add(new PlayerJumpInputSystem())
            .Inject(_controls)
            .Init();

        // Act
        _systems.Run();

        // Assert
        entity.Has<JumpCommand>().Should().BeTrue();
    }

    [Test]
    public void GivenRun_WhenPressingJump_ThenNoJumpCommand()
    {
        // Arrange
        var entity = _world.NewEntity();
        entity.Get<PlayerTag>();
        _controls.JumpPressed.Returns(false);
        _systems.Add(new PlayerJumpInputSystem())
            .Inject(_controls)
            .Init();

        // Act
        _systems.Run();

        // Assert
        entity.Has<JumpCommand>().Should().BeFalse();
    }
}