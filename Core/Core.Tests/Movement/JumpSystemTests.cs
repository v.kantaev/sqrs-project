using System.Numerics;
using Core.Movement;
using Core.Public;
using FluentAssertions;
using Leopotam.Ecs;
using NSubstitute;
using NUnit.Framework;

namespace Core.Tests.Movement;

public class JumpSystemTests
{
    private const float JumpSpeed = 10f;
    private IStaticData _staticData;
    private EcsSystems _systems;
    private EcsWorld _world;

    [SetUp]
    public void SetUp()
    {
        _world = new EcsWorld();
        _systems = new EcsSystems(_world);

        _staticData = Substitute.For<IStaticData>();
        _staticData.JumpSpeed.Returns(JumpSpeed);
    }

    [Test]
    public void GivenRun_WhenVelocityIsPositive_ThenAdd()
    {
        // Arrange
        var entity = _world.NewEntity();
        entity.Get<JumpCommand>();
        entity.Get<VelocityData>().Velocity = new Vector3(0f, 1, 0f);
        _systems.Add(new JumpSystem())
            .Inject(_staticData)
            .Init();

        // Act
        _systems.Run();

        // Assert
        entity.Get<VelocityData>()
            .Velocity
            .Y
            .Should().BeApproximately(JumpSpeed + 1, 0.01f);
    }

    [Test]
    public void GivenRun_WhenVelocityIsNegative_ThenSet()
    {
        // Arrange
        var entity = _world.NewEntity();
        entity.Get<JumpCommand>();
        entity.Get<VelocityData>().Velocity = new Vector3(0f, -1, 0f);
        _systems.Add(new JumpSystem())
            .Inject(_staticData)
            .Init();

        // Act
        _systems.Run();

        // Assert
        entity.Get<VelocityData>()
            .Velocity
            .Y
            .Should().BeApproximately(JumpSpeed, 0.01f);
    }
}