using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Core.Unity;
using Core.Unity.Public.Impl;
using Core.Unity.Skins;
using Leopotam.Ecs;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Tests
{
    public static class IntegrationTestsUtils
    {
        private const int BootstrapSceneIndex = 0;
        private const int GameSceneIndex = 1;

        public static IEnumerator StartGame()
        {
            yield return LoadGameScene();

            TapToPlay();
        }

        public static IEnumerator LoadGameScene()
        {
            SceneManager.LoadScene(BootstrapSceneIndex);
            yield return null;

            yield return new WaitUntil(() => SceneManager.GetActiveScene().buildIndex == GameSceneIndex);
        }

        public static void TapToPlay() => Object.FindObjectOfType<SceneData>()
            .TapToPlay.Play();

        public static Transform GetPlayersSkin(int skinIndex) => GetPlayerSkinComponent()
            .transform.GetChild(skinIndex);

        public static IEnumerable<Transform> GetAllPlayersSkins() => GetPlayerSkinComponent()
            .transform.OfType<Transform>();

        private static PlayerSkin GetPlayerSkinComponent() => Object.FindObjectOfType<PlayerSkin>();

        public static int GetPlayersSkinCount() => GetPlayerSkinComponent().SkinsCount;

        public static IEnumerator WaitForEcsInitialization()
        {
            EcsStartup ecsStartup;

            do
            {
                yield return null;
                ecsStartup = FindEcsStartup();
            } while (ecsStartup == null);

            yield return new WaitUntil(() => ecsStartup.Initialized);
        }

        public static IEnumerator WaitForPlayerDeath()
        {
            yield return WaitForEcsInitialization();

            var findEcsStartup = FindEcsStartup();

            yield return new WaitWhile(() => findEcsStartup.RuntimeData.PlayerEntity.IsAlive());
        }

        public static EcsStartup FindEcsStartup() => Object.FindObjectOfType<EcsStartup>();
    }
}