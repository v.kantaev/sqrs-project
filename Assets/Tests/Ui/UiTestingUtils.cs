using System.Collections;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Tests.Ui
{
    public static class UiTestingUtils
    {
        [MustUseReturnValue]
        public static Canvas FindCanvas() => Object.FindObjectOfType<Canvas>();

        [MustUseReturnValue]
        public static RectTransform FindElement(string path) =>
            (RectTransform) FindCanvas().transform.Find(path);

        [MustUseReturnValue]
        public static EventSystem FindEventSystem() => Object.FindObjectOfType<EventSystem>();

        public static void Click(GameObject gameObject)
        {
            ExecuteEvents.Execute(gameObject, new PointerEventData(FindEventSystem()),
                ExecuteEvents.pointerClickHandler
            );
        }

        [MustUseReturnValue]
        public static IEnumerator UserDelay(float delay = 0.5f) => new WaitForSecondsRealtime(delay);
    }
}