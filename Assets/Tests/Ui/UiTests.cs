using System.Collections;
using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.TestTools;
using static Tests.Ui.UiTestingUtils;
using static Tests.IntegrationTestsUtils;

namespace Tests.Ui
{
    public class UiTests
    {
        private const string TapToPlayPath = "Tap to Play";
        private const string ButtonSkinsMenuPath = "Button Skins Menu";
        private const string SkinsPath = "Skins";
        private const string SkinsButtonsPath = "Skins/SkinButtons";

        [UnitySetUp]
        public IEnumerator SetUp()
        {
            PlayerPrefs.DeleteAll();
            yield return LoadGameScene();
            yield return UserDelay();
        }

        [Test]
        public void GivenLoadedGame_WhenSearchingForEventSystem_ThenItIsOnlyOne()
        {
            // Arrange

            // Act
            var eventSystems = Object.FindObjectsOfType<EventSystem>();

            // Assert
            eventSystems.Should().HaveCount(1);
        }

        [UnityTest]
        public IEnumerator GivenTapToPlayScreen_WhenClicked_ThenGetsHidden()
        {
            // Arrange
            var tapToPlay = FindElement(TapToPlayPath);

            // Act
            Click(tapToPlay.gameObject);
            yield return UserDelay();

            // Assert
            tapToPlay.gameObject.activeInHierarchy.Should().Be(false);
        }

        [UnityTest]
        public IEnumerator GivenGameplayScreen_WhenOpened_ThenSkinsButtonIsNotVisible()
        {
            // Arrange
            var tapToPlay = FindElement(TapToPlayPath);
            var buttonSkinsMenu = FindElement(ButtonSkinsMenuPath);

            // Act
            Click(tapToPlay.gameObject);
            yield return UserDelay();

            // Assert
            buttonSkinsMenu.gameObject.activeInHierarchy.Should().Be(false);
        }

        [Test]
        public void GivenTapToPlayScreen_WhenOpened_ThenSkinsButtonIsVisibleAndSkinMenuIsHidden()
        {
            // Arrange
            var buttonSkinsMenu = FindElement(ButtonSkinsMenuPath);
            var skins = FindElement(SkinsPath);

            // Act

            // Assert
            buttonSkinsMenu.gameObject.activeInHierarchy.Should().Be(true);
            skins.gameObject.activeInHierarchy.Should().Be(false);
        }

        [UnityTest]
        public IEnumerator GivenSkinsButton_WhenClicked_ThenOpensSkinMenu()
        {
            // Arrange
            var buttonSkinsMenu = FindElement(ButtonSkinsMenuPath);
            var skins = FindElement(SkinsPath);

            // Act
            Click(buttonSkinsMenu.gameObject);
            yield return UserDelay();

            // Assert
            skins.gameObject.activeInHierarchy.Should().Be(true);
        }

        [UnityTest]
        public IEnumerator GivenSkinsMenu_WhenClickedClose_ThenItIsClosed()
        {
            // Arrange
            var buttonSkinsMenu = FindElement(ButtonSkinsMenuPath);
            var skins = FindElement(SkinsPath);
            var closeButton = FindElement(SkinsPath + "/CloseButton");
            Click(buttonSkinsMenu.gameObject);
            yield return UserDelay();

            // Act
            Click(closeButton.gameObject);
            yield return UserDelay();

            // Assert
            skins.gameObject.activeInHierarchy.Should().Be(false);
        }

        private static string GetSkinButtonName(int skinIndex) => $"SkinButton_{skinIndex}";

        [UnityTest]
        public IEnumerator GivenSkinsMenu_WhenChoosingSkin_ThenSkinIsWorn()
        {
            // Arrange
            var buttonSkinsMenu = FindElement(ButtonSkinsMenuPath);
            const int skinIndex = 1;
            var skinButton = FindElement(SkinsButtonsPath + $"/{GetSkinButtonName(skinIndex)}/Icon");
            Click(buttonSkinsMenu.gameObject);
            yield return UserDelay();

            // Act
            Click(skinButton.gameObject);
            yield return UserDelay();

            // Assert
            var playersSkin = GetPlayersSkin(skinIndex);
            playersSkin.gameObject.activeInHierarchy.Should().Be(true);
        }

        [UnityTest]
        public IEnumerator GivenSkinsMenu_WhenOpen_ThereAreButtonsForAllSkins()
        {
            // Arrange
            var buttonSkinsMenu = FindElement(ButtonSkinsMenuPath);

            // Act
            Click(buttonSkinsMenu.gameObject);
            yield return UserDelay();

            // Assert
            var skinButtonsRoot = FindElement(SkinsButtonsPath);
            var playersSkinCount = GetPlayersSkinCount();
            var playersSkinButtonNames = skinButtonsRoot.OfType<Transform>().Select(t => t.name);
            var allSkinIndices = Enumerable.Range(0, playersSkinCount);
            playersSkinButtonNames.Should().BeEquivalentTo(
                allSkinIndices.Select(GetSkinButtonName)
            );
        }
    }
}