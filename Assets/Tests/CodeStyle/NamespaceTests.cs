using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using JetBrains.Annotations;
using NUnit.Framework;
using UnityEditor;
using UnityEngine;

namespace Tests.CodeStyle
{
    public class NamespaceTests : CodeStyleTestsBase
    {
        private readonly IEnumerable<TextAsset> _allScriptAssets;


        public NamespaceTests() => _allScriptAssets = LoadAllScriptAssets();

        [Test]
        [TestCaseSource(nameof(GetAllTypes))]
        public void NamespaceShouldStartWithAssemblyName(Type type) =>
            type.Namespace
                .Should().StartWith(type.Assembly.GetName().Name);

        [Test]
        [TestCaseSource(nameof(GetAllTypes))]
        public void NamespaceShouldBeConsistentWithFolderLocation(Type type)
        {
            var textAsset = FindScriptAssetOrDefault(type, _allScriptAssets);
            if (textAsset == null) return;

            var path = AssetDatabase.GetAssetPath(textAsset);
            path = path.Replace("Scripts", "");
            path = path.Replace("Assets", "");
            path = path.Replace(".cs", "");
            path = path.Replace(" ", "_");

            var expectedNamespaceParts = path.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
            var expectedNamespace = string.Join(".", expectedNamespaceParts.Take(expectedNamespaceParts.Length - 1));

            type.Namespace
                .Should().Be(expectedNamespace);
        }

        private static TextAsset FindScriptAssetOrDefault(Type type, IEnumerable<TextAsset> allTextAssets) =>
            allTextAssets.FirstOrDefault(ta => TypeIsDefinedIn(type, ta));

        private static IEnumerable<TextAsset> LoadAllScriptAssets() =>
            AssetDatabase.FindAssets("t: MonoScript")
                .Select(AssetDatabase.GUIDToAssetPath)
                .Where(p => !p.StartsWith("Assets/Plugins/") && !p.StartsWith("Assets/Graphics/_External Assets"))
                .Select(AssetDatabase.LoadAssetAtPath<MonoScript>)
                .Where(a => a != null);

        private static bool TypeIsDefinedIn([NotNull] Type type, [NotNull] TextAsset textAsset)
        {
            if (type == null) throw new ArgumentNullException(nameof(type));
            if (textAsset == null) throw new ArgumentNullException(nameof(textAsset));

            var @namespace = type.Namespace ?? string.Empty;

            var lines = textAsset
                    .text
                    .Split(new[] { "\r\n", "\n" }, StringSplitOptions.RemoveEmptyEntries)
                ;
            return lines.Any(l => l.Contains("namespace") && l.Contains(@namespace)) &&
                   lines.Any(l => l.Contains(type.Name.WithoutGenericParameters()));
        }
    }
}