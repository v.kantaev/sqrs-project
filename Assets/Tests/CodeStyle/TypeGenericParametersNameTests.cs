using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using NUnit.Framework;

namespace Tests.CodeStyle
{
    public class TypeGenericParametersNameTests : CodeStyleTestsBase
    {
        private static IEnumerable<object[]> GetAllTypeGenericParameters() =>
            GetAllTypes()
                .Where(t => t.IsGenericTypeDefinition)
                .SelectMany(t => t.GetGenericArguments().Select(a => new object[] { t, a }));

        [Test]
        [TestCaseSource(nameof(GetAllTypeGenericParameters))]
        public void TypeGenericParameterNameShouldBePascalCaseUnderscoreIntolerant(Type type, Type genericParameter) =>
            genericParameter.Name
                .Should().Match(p => p.IsPascalCaseUnderscoreIntolerant() && p.StartsWith("T"));
    }
}