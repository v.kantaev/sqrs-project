﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using FluentAssertions;
using NUnit.Framework;

namespace Tests.CodeStyle
{
    public class EventNameTests : CodeStyleTestsBase
    {
        private static IEnumerable<object[]> GetAllTypeEvents() =>
            GetAllTypes()
                .SelectMany(t =>
                    t.GetEvents(DefaultBindingFlags)
                        .Where(f => !IsCompilerGenerated(f))
                        .Where(e => e.DeclaringType == t)
                        .Where(e => !e.IsSpecialName)
                        .Select(e => new object[] { t, e })
                );

        [Test]
        [TestCaseSource(nameof(GetAllTypeEvents))]
        public void EventNameShouldBePascalCaseUnderscoreIntolerant(Type type, EventInfo @event) =>
            @event.Name
                .Should().Match(m => m.IsPascalCaseUnderscoreIntolerant());
    }
}