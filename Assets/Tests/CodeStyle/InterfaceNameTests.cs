﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using NUnit.Framework;

namespace Tests.CodeStyle
{
    public class InterfaceNameTests : CodeStyleTestsBase
    {
        private static IEnumerable<Type> GetAllInterfaces() =>
            GetAllTypes()
                .Where(t => t.IsInterface);

        [Test]
        [TestCaseSource(nameof(GetAllInterfaces))]
        public void InterfaceNameShouldStartWithCapitalI(Type type) =>
            type.Name.Should().StartWith("I");
    }
}