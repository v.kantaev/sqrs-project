using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using FluentAssertions;
using NUnit.Framework;

namespace Tests.CodeStyle
{
    public class FieldNameTests : CodeStyleTestsBase
    {
        public static IEnumerable<object[]> GetAllPrivateNonStaticFields() =>
            GetAllTypeFields()
                .Where(f =>
                    {
                        var field = (FieldInfo) f[1];
                        return field.IsPrivate && !field.IsStatic;
                    }
                );

        public static IEnumerable<object[]> GetAllPrivateStaticNonReadonlyFields() =>
            GetAllTypeFields()
                .Where(f =>
                    {
                        var field = (FieldInfo) f[1];
                        return field.IsPrivate && field.IsStatic && !field.IsInitOnly && !field.IsLiteral;
                    }
                );

        public static IEnumerable<object[]> GetAllNonPrivateAndStaticReadonlyFields() =>
            GetAllTypeFields()
                .Where(f =>
                    {
                        var field = (FieldInfo) f[1];
                        return !field.IsPrivate || field.IsStatic && field.IsInitOnly || field.IsLiteral;
                    }
                );

        [TestCaseSource(nameof(GetAllPrivateNonStaticFields))]
        public void PrivateNonStaticFieldNameShouldBeCamelCaseStartingWithUnderscore(Type type, FieldInfo field) =>
            field.Name
                .Should().Match(m => m.IsCamelCaseStartingWithUnderscore());

        [TestCaseSource(nameof(GetAllPrivateStaticNonReadonlyFields))]
        public void PrivateStaticNonReadonlyFieldNameShouldBeCamelCaseStartingWithUnderscore(Type type,
            FieldInfo field) =>
            field.Name
                .Should().Match(m => m.IsCamelCaseStartingWithUnderscore());

        [TestCaseSource(nameof(GetAllNonPrivateAndStaticReadonlyFields))]
        public void NonPrivateAndStaticReadonlyFieldNameShouldBePascalCase(Type type, FieldInfo field) =>
            field.Name
                .Should().Match(m => m.IsPascalCaseUnderscoreIntolerant());
    }
}