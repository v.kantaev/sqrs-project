﻿using System;
using System.Linq;
using System.Reflection;
using JetBrains.Annotations;

namespace Tests.CodeStyle
{
    internal static class CodeStyleExtensions
    {
        public static bool IsPascalCaseUnderscoreTolerant([NotNull] this string identifier)
        {
            if (identifier == null) throw new ArgumentNullException(nameof(identifier));
            return identifier
                .Split('_')
                .All(s => s.Length > 0 && char.IsUpper(s[0]));
        }

        public static bool IsPascalCaseUnderscoreIntolerant([NotNull] this string identifier)
        {
            if (identifier == null) throw new ArgumentNullException(nameof(identifier));
            if (identifier.Contains('_')) return false;
            return identifier.Length > 0 && char.IsUpper(identifier[0]);
        }

        public static bool IsCamelCaseStartingWithUnderscore([NotNull] this string identifier)
        {
            if (identifier == null) throw new ArgumentNullException(nameof(identifier));
            return identifier.StartsWith("_") && IsCamelCase(identifier.Substring(1));
        }

        public static bool IsCamelCase([NotNull] this string identifier)
        {
            if (identifier == null) throw new ArgumentNullException(nameof(identifier));
            return !identifier.Contains('_') && identifier.Length > 0 && char.IsLower(identifier[0]);
        }

        public static string WithoutGenericParameters(this string identifier)
        {
            var openingBracketIndex = identifier.IndexOf("<", StringComparison.InvariantCulture);
            if (openingBracketIndex == -1) return identifier;
            return identifier.Substring(0, openingBracketIndex);
        }

        public static string PrettyName(this MemberInfo memberInfo)
        {
            Type[] genericArguments;

            switch (memberInfo)
            {
                case Type type:
                    genericArguments = type.GetGenericArguments();
                    break;

                case MethodInfo method:
                    genericArguments = method.GetGenericArguments();
                    break;

                default:
                    return memberInfo.Name;
            }

            if (genericArguments.Length == 0) return memberInfo.Name;

            var name = memberInfo.Name;
            var unmangledName =
                name.Substring(0, name.IndexOf("`", StringComparison.InvariantCulture));
            return unmangledName + "<" + string.Join(",", genericArguments.Select(PrettyName)) + ">";
        }
    }
}