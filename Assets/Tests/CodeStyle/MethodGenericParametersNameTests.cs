﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using FluentAssertions;
using NUnit.Framework;

namespace Tests.CodeStyle
{
    public class MethodGenericParametersNameTests : CodeStyleTestsBase
    {
        private static IEnumerable<object[]> GetAllMethodGenericParameters() =>
            GetAllTypeMethods()
                .Where(m =>
                    {
                        var method = (MethodInfo) m[1];
                        return method.IsGenericMethodDefinition;
                    }
                )
                .SelectMany(m =>
                    {
                        var type = (Type) m[0];
                        var method = (MethodInfo) m[1];
                        return method.GetGenericArguments().Select(a => new object[] { type, a });
                    }
                );

        [Test]
        [TestCaseSource(nameof(GetAllMethodGenericParameters))]
        public void MethodGenericParameterNameShouldBePascalCaseUnderscoreIntolerant(Type type,
            Type genericParameter) =>
            genericParameter.Name
                .Should().Match(p => p.IsPascalCaseUnderscoreIntolerant() && p.StartsWith("T"));
    }
}