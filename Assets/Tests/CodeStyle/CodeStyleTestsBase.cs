﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using Core.Unity;
using FluentAssertions;
using Tests.CodeStyle.Examples;

namespace Tests.CodeStyle
{
    public abstract class CodeStyleTestsBase
    {
        protected static BindingFlags DefaultBindingFlags =>
            BindingFlags.Instance | BindingFlags.Static |
            BindingFlags.Public | BindingFlags.NonPublic;

        protected static IEnumerable<Type> GetAllTypes() =>
            Assembly.GetAssembly(typeof(CoreUnityAssemblyAnchor)).Types()
                .Concat(Assembly.GetAssembly(typeof(CodeStyleExampleClass)).Types())
                .Where(f => !IsCompilerGenerated(f))
                .Where(t => !IsSpecialName(t.Name) && !t.IsConstructedGenericType);

        private static bool IsSpecialName(string name)
        {
            var invalidChars = new[] { '<', '>' };
            return name.Any(invalidChars.Contains);
        }

        protected static IEnumerable<object[]> GetAllTypeMethods() =>
            GetAllTypes()
                .SelectMany(t =>
                    t.GetMethods(DefaultBindingFlags)
                        .Where(f => !IsCompilerGenerated(f))
                        .Where(m => m.DeclaringType == t)
                        .Where(m => !m.IsSpecialName && !IsSpecialName(m.Name))
                        .Select(m => new object[] { t, m })
                );

        protected static IEnumerable<object[]> GetAllTypeFields() =>
            GetAllTypes()
                .SelectMany(t =>
                    t.GetFields(DefaultBindingFlags)
                        .Where(f => !IsCompilerGenerated(f))
                        .Where(f => f.DeclaringType == t)
                        .Where(f => !f.IsSpecialName)
                        .Where(f => !IsSpecialName(f.Name))
                        .Select(f => new object[] { t, f })
                );

        protected static bool IsCompilerGenerated(MemberInfo memberInfo) =>
            Attribute.IsDefined(memberInfo, typeof(CompilerGeneratedAttribute));
    }
}