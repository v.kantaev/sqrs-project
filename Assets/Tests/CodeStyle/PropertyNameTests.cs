﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using FluentAssertions;
using NUnit.Framework;

namespace Tests.CodeStyle
{
    public class PropertyNameTests : CodeStyleTestsBase
    {
        private static IEnumerable<object[]> GetAllTypeProperties() =>
            GetAllTypes()
                .SelectMany(t =>
                    t.GetProperties(DefaultBindingFlags)
                        .Where(f => !IsCompilerGenerated(f))
                        .Where(p => p.DeclaringType == t)
                        .Where(p => !p.IsSpecialName)
                        .Select(p => new object[] { t, p })
                );

        [Test]
        [TestCaseSource(nameof(GetAllTypeProperties))]
        public void PropertyNameShouldBePascalCaseUnderscoreIntolerant(Type type, PropertyInfo property) =>
            property.Name
                .Should().Match(m => m.IsPascalCaseUnderscoreIntolerant());
    }
}