using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using FluentAssertions;
using NUnit.Framework;

namespace Tests.CodeStyle
{
    public class MethodParametersNameTests : CodeStyleTestsBase
    {
        private static IEnumerable<object[]> GetAllTypeMethodParameters() =>
            GetAllTypeMethods()
                .SelectMany(x =>
                    {
                        var type = (Type) x[0];
                        var method = (MethodInfo) x[1];
                        return method.GetParameters().Select(p => new object[] { type, p });
                    }
                );

        [Test]
        [TestCaseSource(nameof(GetAllTypeMethodParameters))]
        public void MethodParameterNameShouldBePascalCaseUnderscoreIntolerant(Type type, ParameterInfo parameter) =>
            parameter.Name
                .Should().Match(m => m.IsCamelCase());
    }
}