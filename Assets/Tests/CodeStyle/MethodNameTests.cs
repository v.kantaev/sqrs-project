﻿using System;
using System.Reflection;
using FluentAssertions;
using NUnit.Framework;

namespace Tests.CodeStyle
{
    public class MethodNameTests : CodeStyleTestsBase
    {
        [Test]
        [TestCaseSource(nameof(GetAllTypeMethods))]
        public void MethodNameShouldBePascalCaseUnderscoreTolerant(Type type, MethodInfo method) =>
            method.Name.WithoutGenericParameters()
                .Should().Match(m => m.IsPascalCaseUnderscoreTolerant());
    }
}