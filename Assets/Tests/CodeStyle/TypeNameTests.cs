﻿using System;
using FluentAssertions;
using NUnit.Framework;

namespace Tests.CodeStyle
{
    public class TypeNameTests : CodeStyleTestsBase
    {
        [Test]
        [TestCaseSource(nameof(GetAllTypes))]
        public void TypeNameShouldBePascalCaseUnderscoreTolerant(Type type) =>
            type.PrettyName().WithoutGenericParameters()
                .Should().Match(s => s.IsPascalCaseUnderscoreTolerant());
    }
}