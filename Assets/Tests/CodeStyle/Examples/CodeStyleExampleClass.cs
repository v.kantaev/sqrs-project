﻿using System;
using JetBrains.Annotations;
using UnityEngine;

#pragma warning disable 0067
#pragma warning disable 0169

namespace Tests.CodeStyle.Examples
{
    [UsedImplicitly]
    internal class CodeStyleExampleClass
    {
        public const int D = 1;
        private static int _b;
        private static readonly int C;
        private float _a;

        public int E = 4;


        public int Property { get; set; }

        public void Method()
        {
            // example of a anonymous type/method, should not be tested
            // ReSharper disable once UnusedVariable
            Action lambda = () => { Debug.Log(this); };
            var a = new Class<int, float>();
        }

        public void Method_Underscore(int parameterName) { }

        public void GenericMethod<T, TSecond>() { }

        public event Action Event;

        // ReSharper disable once InconsistentNaming
        public class Example_Underscore { }

        public interface IInterfaceExample { }

        private class Class<T, TSecond> { }
    }
}