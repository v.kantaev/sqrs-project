using System.Collections;
using System.Linq;
using FluentAssertions;
using Unity.PerformanceTesting;
using UnityEngine.Profiling;
using UnityEngine.TestTools;
using static Tests.IntegrationTestsUtils;

namespace Tests.Performance
{
    public class PerformanceTests
    {
        private const float MinimumFps = 55f;
        private const float DesiredFrameTimeMs = 1000f / MinimumFps;
        private const float MaximumMemoryMb = 500f;
        private const int WarmUpCount = 60;
        private const int MeasurementCount = 90;

        [UnitySetUp]
        public IEnumerator SetUp()
        {
            yield return StartGame();
        }

        [UnityTest]
        [Performance]
        public IEnumerator FrameTimeTest()
        {
            TapToPlay();

            yield return Measure
                .Frames()
                .WarmupCount(WarmUpCount)
                .MeasurementCount(MeasurementCount)
                .Run();

            var info = CalculateStatisticalValues();

            var frameTime = info.SampleGroups.First(sg => sg.Name == "Time");
            frameTime.Max.Should().BeLessOrEqualTo(DesiredFrameTimeMs);
        }

        private static PerformanceTest CalculateStatisticalValues()
        {
            var info = PerformanceTest.Active;
            info.CalculateStatisticalValues();
            return info;
        }

        [UnityTest]
        [Performance]
        public IEnumerator MemoryTest()
        {
            TapToPlay();

            yield return Measure
                .Frames()
                .WarmupCount(WarmUpCount)
                .MeasurementCount(MeasurementCount)
                .Run();

            var memorySampleGroup = new SampleGroup("TotalAllocatedMemory", SampleUnit.Megabyte);
            var allocatedMemory = Profiler.GetTotalAllocatedMemoryLong() / 1048576f;
            Measure.Custom(memorySampleGroup, allocatedMemory);

            var info = CalculateStatisticalValues();
            var memoryMb = info.SampleGroups.First(sg => sg.Name == memorySampleGroup.Name);
            memoryMb.Max.Should().BeLessOrEqualTo(MaximumMemoryMb);
        }
    }
}