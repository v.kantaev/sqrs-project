using System.Collections;
using FluentAssertions;
using UnityEngine.TestTools;

namespace Tests.Death
{
    public class PlayerDeathCameraSystemTests
    {
        [UnityTest]
        public IEnumerator GivenGame_WhenPlayerDies_ThenCameraFollowIsNull()
        {
            // Arrange
            yield return IntegrationTestsUtils.StartGame();
            yield return IntegrationTestsUtils.WaitForPlayerDeath();

            // Act
            var ecsStartup = IntegrationTestsUtils.FindEcsStartup();
            var follow = ecsStartup.SceneData.PlayerCamera.Follow;

            // Assert
            follow.Should().BeNull();
        }
    }
}