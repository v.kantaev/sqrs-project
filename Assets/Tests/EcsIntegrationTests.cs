﻿using System.Collections;
using Core.Unity;
using FluentAssertions;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;

namespace Tests
{
    public class EcsIntegrationTests
    {
        [UnityTest]
        public IEnumerator GivenBootstrapScene_WhenStarting_ThenAtGameAndContainsEcsStartup()
        {
            // Arrange
            yield return IntegrationTestsUtils.StartGame();

            // Act

            // Assert
            SceneManager.GetActiveScene().buildIndex
                .Should().Be(1);

            Object.FindObjectOfType<EcsStartup>()
                .Should().NotBe(null);
        }
    }
}