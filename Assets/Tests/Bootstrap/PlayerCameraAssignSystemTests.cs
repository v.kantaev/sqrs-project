using System.Collections;
using FluentAssertions;
using UnityEngine.TestTools;

namespace Tests.Bootstrap
{
    public class PlayerCameraAssignSystemTests
    {
        [UnityTest]
        public IEnumerator GivenGame_WhenStarted_ThenPlayerCameraIsAssigned()
        {
            // Arrange
            yield return IntegrationTestsUtils.StartGame();
            yield return IntegrationTestsUtils.WaitForEcsInitialization();

            // Act
            var ecsStartup = IntegrationTestsUtils.FindEcsStartup();
            var follow = ecsStartup.SceneData.PlayerCamera.Follow;

            // Assert
            follow.Should().NotBeNull();
        }
    }
}