using System;
using System.Collections.Generic;
using PlayFab;
using PlayFab.ClientModels;
using UnityEngine;

namespace Scoreboard
{
    public class PlayfabManager : MonoBehaviour
    {
        public string PlayFabId { get; private set; }

        // Start is called before the first frame update
        private void Start()
        {
            DontDestroyOnLoad(this);
            Login();
        }

        private void Login()
        {
            var request = new LoginWithCustomIDRequest
            {
                CustomId = SystemInfo.deviceUniqueIdentifier,
                CreateAccount = true,
            };
            PlayFabClientAPI.LoginWithCustomID(request, OnSuccess, OnError);
        }

        private void OnSuccess(LoginResult result)
        {
            PlayFabId = result.PlayFabId;
            Debug.Log("Successful login create");
            Success?.Invoke();
        }

        public event Action Success;

        private void OnError(PlayFabError error)
        {
            Debug.Log("Error while logging");
            Debug.Log(error.GenerateErrorReport());
            Error?.Invoke(error);
        }

        public event Action<PlayFabError> Error;

        public void SendLeaderBoard(int score)
        {
            var request = new UpdatePlayerStatisticsRequest
            {
                Statistics = new List<StatisticUpdate>
                {
                    new StatisticUpdate
                    {
                        StatisticName = "PlatformScore",
                        Value = score,
                    },
                },
            };
            PlayFabClientAPI.UpdatePlayerStatistics(request, OnLeaderBoardUpdate, OnError);
        }

        private void OnLeaderBoardUpdate(UpdatePlayerStatisticsResult result)
        {
            Debug.Log("Successfuly sent to the leaderboard");
        }

        public void GetLeaderBoard(int maxResults, Action<GetLeaderboardResult> onSuccess, Action<PlayFabError> onError)
        {
            var request = new GetLeaderboardRequest
            {
                StatisticName = "PlatformScore",
                StartPosition = 0,
                MaxResultsCount = maxResults,
            };
            PlayFabClientAPI.GetLeaderboard(request, onSuccess, onError);
        }
    }
}