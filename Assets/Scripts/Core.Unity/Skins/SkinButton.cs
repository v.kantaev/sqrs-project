﻿using Core.Skins;
using Leopotam.Ecs;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Unity.Skins
{
    public class SkinButton : MonoBehaviour
    {
        [SerializeField] private Button _button;
        [SerializeField] private Text _text;

        private int _skinIndex;
        private EcsWorld _world;

        private void OnEnable()
        {
            _button.onClick.AddListener(OnClick);
        }

        private void OnDisable()
        {
            _button.onClick.RemoveListener(OnClick);
        }

        private void OnClick()
        {
            _world.NewEntity().Get<SetSkinCommand>().SkinIndex = _skinIndex;
        }

        public void Init(EcsWorld world, int skinIndex)
        {
            _skinIndex = skinIndex;
            _world = world;
            _text.text = (skinIndex + 1).ToString();
        }
    }
}