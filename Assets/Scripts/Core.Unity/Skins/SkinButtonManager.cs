using Core.Skins;
using Leopotam.Ecs;
using UnityEngine;

namespace Core.Unity.Skins
{
    public class SkinButtonManager : MonoBehaviour, ISkinButtonManager
    {
        [SerializeField] private SkinButton _skinButtonPrefab;

        public void Init(IPlayerSkinModel playerSkinModel, EcsWorld world)
        {
            for (var skinIndex = 0; skinIndex < playerSkinModel.SkinsCount; skinIndex++)
            {
                var skinButton = Instantiate(_skinButtonPrefab, transform);
                skinButton.name = $"SkinButton_{skinIndex}";
                skinButton.Init(world, skinIndex);
            }
        }
    }
}