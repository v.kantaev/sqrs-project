﻿using UnityEngine;

namespace Core.Unity.Skins
{
    public class SkinMenuButton : MonoBehaviour
    {
        [SerializeField] private GameObject _skinMenu;

        public void OpenMenu()
        {
            _skinMenu.SetActive(true);
        }
    }
}