﻿using UnityEngine;

namespace Core.Unity.Skins
{
    public class PlayerSkin : MonoBehaviour
    {
        [SerializeField] private GameObject[] _skins;

        public int SkinsCount => _skins.Length;

        public void SetSkin(int skinNumber)
        {
            foreach (var skin in _skins)
            {
                skin.SetActive(false);
            }

            _skins[skinNumber].SetActive(true);
        }

        public void HideAllSkins()
        {
            foreach (var skin in _skins)
            {
                skin.SetActive(false);
            }
        }
    }
}