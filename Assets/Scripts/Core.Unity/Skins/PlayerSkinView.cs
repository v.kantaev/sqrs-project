using Core.Public;
using Core.Skins;
using Core.Unity.Public.Impl;
using Leopotam.Ecs;

namespace Core.Unity.Skins
{
    public class PlayerSkinView : IPlayerSkinView
    {
        private readonly RuntimeData _runtimeData;
        private int _currentSkinIndex;

        public PlayerSkinView(RuntimeData runtimeData) => _runtimeData = runtimeData;

        public bool CanSetSkin() => _runtimeData.PlayerEntity.IsAlive();

        public void SetSkin(int skinIndex)
        {
            var playerRef = _runtimeData.PlayerEntity.Get<GameObjectRef>();
            playerRef.GameObject.GetComponent<PlayerSkin>().SetSkin(skinIndex);
            _currentSkinIndex = skinIndex;
        }

        public void Hide()
        {
            var playerEntity = _runtimeData.PlayerEntity;
            if (!playerEntity.IsAlive()) return;

            var playerRef = playerEntity.Get<GameObjectRef>();
            playerRef.GameObject.GetComponent<PlayerSkin>().HideAllSkins();
        }

        public void Show()
        {
            var playerEntity = _runtimeData.PlayerEntity;
            if (!playerEntity.IsAlive()) return;

            var playerRef = playerEntity.Get<GameObjectRef>();
            playerRef.GameObject.GetComponent<PlayerSkin>().SetSkin(_currentSkinIndex);
        }
    }
}