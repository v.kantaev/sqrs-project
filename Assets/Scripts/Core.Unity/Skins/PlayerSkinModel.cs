using Core.Skins;
using Core.Unity.Public.Impl;
using UnityEngine;

namespace Core.Unity.Skins
{
    public class PlayerSkinModel : IPlayerSkinModel
    {
        private const string Key = nameof(PlayerSkinModel) + "_" + nameof(SkinIndex);

        public PlayerSkinModel(StaticData staticData) =>
            SkinsCount = staticData.PlayerPrefab.GetComponent<PlayerSkin>().SkinsCount;

        public int SkinIndex
        {
            get => PlayerPrefs.GetInt(Key, 0);
            set
            {
                PlayerPrefs.SetInt(Key, value);
                PlayerPrefs.Save();
            }
        }

        public int SkinsCount { get; }
    }
}