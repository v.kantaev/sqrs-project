using UnityEngine;

namespace Core.Unity.Numerics
{
    public static class NumericsExtensions
    {
        public static Vector3 ToUnityVector(this System.Numerics.Vector3 vector3) =>
            new Vector3(vector3.X, vector3.Y, vector3.Z);

        public static System.Numerics.Vector3 ToNumericsVector(this Vector3 vector3) =>
            new System.Numerics.Vector3(vector3.x, vector3.y, vector3.z);

        public static Quaternion ToUnityQuaternion(this System.Numerics.Quaternion quaternion) =>
            new Quaternion(quaternion.X, quaternion.Y, quaternion.Z, quaternion.W);
    }
}