using System;
using System.Collections.Generic;
using System.Linq;
using Core.Score;
using Scoreboard;

namespace Core.Unity.Scoreboard
{
    public class PlayFabScoreboardService : IScoreboardService
    {
        private readonly PlayfabManager _playfabManager;

        public PlayFabScoreboardService(PlayfabManager playfabManager) => _playfabManager = playfabManager;

        public void SendScore(int score) => _playfabManager.SendLeaderBoard(score);

        public void GetLeaderboard(int maxResults, Action<IEnumerable<ScoreboardEntry>> onSuccess,
            Action<string> onError)
        {
            _playfabManager.GetLeaderBoard(maxResults, glr =>
                {
                    onSuccess(glr.Leaderboard.Select(i => new ScoreboardEntry
                            {
                                Score = i.StatValue,
                                Position = i.Position,
                                UserId = i.PlayFabId,
                            }
                        )
                    );
                }, error => { onError(error.Error.ToString()); }
            );
        }

        public string GetUserId() => _playfabManager.PlayFabId;
    }
}