using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Core.Unity.Scoreboard
{
    public class LoseScreen : MonoBehaviour
    {
        [SerializeField] private Button _button;
        [SerializeField] private Text _scoreText;

        public void Open(int score, UnityAction restart)
        {
            _scoreText.text = "Your score: " + score;
            _button.onClick.AddListener(restart);
        }
    }
}