using UnityEngine;
using UnityEngine.UI;

namespace Core.Unity.Scoreboard
{
    public class ScoreboardScreenEntry : MonoBehaviour
    {
        [SerializeField] private Text _placeText;
        [SerializeField] private Text _nameText;
        [SerializeField] private Text _scoreText;

        public void Hide() => gameObject.SetActive(false);

        public void Init(int place, string userName, int score)
        {
            _placeText.text = FormatPlace(place);
            _nameText.text = userName;
            _scoreText.text = score.ToString();
            gameObject.SetActive(true);
        }

        private static string FormatPlace(int place) =>
            place switch
            {
                1 => "1st",
                2 => "2nd",
                3 => "3rd",
                _ => $"{place}th",
            };
    }
}