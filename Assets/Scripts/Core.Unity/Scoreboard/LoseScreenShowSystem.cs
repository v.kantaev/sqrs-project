using Core.Death;
using Core.Score;
using Core.Unity.Public.Impl;
using Leopotam.Ecs;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Core.Unity.Scoreboard
{
    public class LoseScreenShowSystem : IEcsRunSystem
    {
        private readonly EcsFilter<PlayerDeathEvent> _filter = default;
        private readonly SceneData _sceneData = default;
        private readonly ScoreModel _scoreModel = default;
        private readonly StaticData _staticData = default;

        public void Run()
        {
            if (_filter.GetEntitiesCount() == 0) return;

            var loseScreen = Object.Instantiate(_staticData.LoseScreenPrefab, _sceneData.Canvas.transform);
            loseScreen.Open(_scoreModel.Score, () =>
                {
                    var buildIndex = SceneManager.GetActiveScene().buildIndex;
                    SceneManager.LoadScene(buildIndex);
                }
            );
        }
    }
}