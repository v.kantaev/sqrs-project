using Core.Score;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Unity.Scoreboard
{
    public class ScoreView : MonoBehaviour
    {
        [SerializeField] private EcsStartup _startup;
        [SerializeField] private Text _text;

        private ScoreModel _scoreModel;

        private void Start()
        {
            _scoreModel = _startup.ScoreModel;
            Refresh();
            _scoreModel.Changed += Refresh;
        }

        private void OnDestroy()
        {
            if (_scoreModel != null)
                _scoreModel.Changed -= Refresh;
        }

        private void Refresh()
        {
            _text.text = _scoreModel.Score.ToString();
        }
    }
}