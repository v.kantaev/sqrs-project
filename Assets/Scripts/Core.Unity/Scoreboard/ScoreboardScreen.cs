using System.Collections.Generic;
using Core.Score;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Unity.Scoreboard
{
    public class ScoreboardScreen : MonoBehaviour
    {
        [SerializeField] private EcsStartup _ecsStartup;
        [SerializeField] private int _maxResults = 10;
        [SerializeField] private ScoreboardScreenEntry _scoreboardScreenEntryPrefab;
        [SerializeField] private GameObject _loadingLayout;
        [SerializeField] private GameObject _scoreboardLayout;
        [SerializeField] private GameObject _errorLayout;
        [SerializeField] private Transform _entriesRoot;
        [SerializeField]
        private Text _errorText;
        [SerializeField] private Button _refreshButton;

        private ScoreboardScreenEntry[] _scoreboardScreenEntries;

        private IScoreboardService ScoreboardService => _ecsStartup.ScoreboardService;

        private void Awake()
        {
            _scoreboardScreenEntries = new ScoreboardScreenEntry[_maxResults];
            for (var i = 0; i < _maxResults; i++)
            {
                _scoreboardScreenEntries[i] = Instantiate(_scoreboardScreenEntryPrefab, _entriesRoot);
            }
        }

        private void Start()
        {
            Load();
        }

        public void Load()
        {
            _refreshButton.interactable = false;
            _loadingLayout.SetActive(true);
            _scoreboardLayout.SetActive(false);
            _errorLayout.SetActive(false);

            ScoreboardService.GetLeaderboard(_maxResults, OnGotResults, OnGotError);
        }

        private void OnGotResults(IEnumerable<ScoreboardEntry> entries)
        {
            _loadingLayout.SetActive(false);
            _scoreboardLayout.SetActive(true);
            _errorLayout.SetActive(false);
            _refreshButton.interactable = true;

            var index = 0;

            foreach (var entry in entries)
            {
                var scoreboardScreenEntry = _scoreboardScreenEntries[index];
                var place = entry.Position + 1;
                scoreboardScreenEntry.Init(place, GetDisplayName(entry.UserId), entry.Score);
                index++;
            }

            for (var i = index; i < _scoreboardScreenEntries.Length; i++)
            {
                _scoreboardScreenEntries[i].Hide();
            }
        }

        private string GetDisplayName(string id)
        {
            if (id == ScoreboardService.GetUserId())
                return "You";

            const int charsFromId = 4;
            return "User" + id.Substring(id.Length - charsFromId);
        }

        private void OnGotError(string error)
        {
            _loadingLayout.SetActive(false);
            _scoreboardLayout.SetActive(false);
            _errorLayout.SetActive(true);
            _refreshButton.interactable = true;

            _errorText.text = $"Error: {error}";
        }
    }
}