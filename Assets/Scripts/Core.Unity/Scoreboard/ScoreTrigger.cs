using Core.Bootstrap;
using Core.Score;
using Core.Unity.Public.Impl;
using Leopotam.Ecs;
using UnityEngine;

namespace Core.Unity.Scoreboard
{
    public class ScoreTrigger : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            var entityBehavior = other.GetComponentInParent<EcsEntityBehavior>();
            if (entityBehavior == null) return;
            if (!entityBehavior.Entity.IsAlive()) return;
            if (!entityBehavior.Entity.Has<PlayerTag>()) return;

            var world = entityBehavior.Entity.GetInternalWorld();
            world.NewEntity().Get<IncreaseScoreCommand>();
        }
    }
}