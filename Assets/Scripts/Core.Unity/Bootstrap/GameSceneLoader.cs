﻿using System.Threading.Tasks;
using PlayFab;
using Scoreboard;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Core.Unity.Bootstrap
{
    public class GameSceneLoader : MonoBehaviour
    {
        [SerializeField] [Min(0)] private int _sceneIndex = 1;
        [SerializeField] private PlayfabManager _playfabManager;
        [SerializeField] private Text _text;

        private TaskCompletionSource<Result> _taskCompletionSource;

        private static bool Loaded { get; set; }

        private async void Start()
        {
            _taskCompletionSource = new TaskCompletionSource<Result>();
            _playfabManager.Error += OnError;
            _playfabManager.Success += OnSuccess;

            var result = await _taskCompletionSource.Task;
            if (result == Result.Success)
            {
                Loaded = true;
                SceneManager.LoadScene(_sceneIndex);
            }
            else
            {
                _text.text = "Can't establish connection. Try again later.";
            }
        }

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        private static void Init()
        {
            Loaded = false;
        }

        public static bool EnsureLoaded()
        {
            if (Loaded) return true;

            const int bootstrapSceneIndex = 0;
            SceneManager.LoadScene(bootstrapSceneIndex);
            return false;
        }

        private void OnSuccess()
        {
            _taskCompletionSource.TrySetResult(Result.Success);
        }

        private void OnError(PlayFabError obj)
        {
            _taskCompletionSource.TrySetResult(Result.Error);
        }

        private enum Result
        {
            Success,
            Error,
        }
    }
}