using Cinemachine;
using UnityEngine;

namespace Core.Unity.Bootstrap
{
    public class CinemachineFixPositionExtension : CinemachineExtension
    {
        [SerializeField] private bool _fixX;
        [SerializeField] private float _x;
        [SerializeField] private bool _fixY;
        [SerializeField] private float _y;

        protected override void PostPipelineStageCallback(CinemachineVirtualCameraBase vcam,
            CinemachineCore.Stage stage, ref CameraState state, float deltaTime)
        {
            if (stage != CinemachineCore.Stage.Finalize) return;
            var rawPosition = state.RawPosition;
            if (_fixX)
                rawPosition.x = _x;
            if (_fixY)
                rawPosition.y = _y;
            state.RawPosition = rawPosition;
        }
    }
}