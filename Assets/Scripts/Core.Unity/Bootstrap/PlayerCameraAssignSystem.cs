using Core.Public;
using Core.Unity.Public.Impl;
using Leopotam.Ecs;

namespace Core.Unity.Bootstrap
{
    public class PlayerCameraAssignSystem : IEcsInitSystem
    {
        private readonly RuntimeData _runtimeData = default;
        private readonly SceneData _sceneData = default;

        public void Init()
        {
            var transform = _runtimeData.PlayerEntity.Get<GameObjectRef>().GameObject.transform;
            _sceneData.PlayerCamera.Follow = transform;
        }
    }
}