﻿using Core.Bonuses;
using Core.Bootstrap;
using Core.Death;
using Core.LevelGeneration;
using Core.Movement;
using Core.Public;
using Core.Score;
using Core.Skins;
using Core.Unity.Bonus;
using Core.Unity.Bootstrap;
using Core.Unity.Death;
using Core.Unity.LevelGeneration;
using Core.Unity.Movement;
using Core.Unity.Public.Impl;
using Core.Unity.Scoreboard;
using Core.Unity.Skins;
using Core.Unity.TimeManagement;
using Leopotam.Ecs;
using Scoreboard;
using UnityEngine;

namespace Core.Unity
{
    public class EcsStartup : MonoBehaviour
    {
        [SerializeField] private StaticData _staticData;
        [SerializeField] private SceneData _sceneData;
        [SerializeField] private DeathZone _deathZone;

        private EcsSystems _systems;
        private EcsWorld _world;
        public IScoreboardService ScoreboardService { get; private set; }
        public RuntimeData RuntimeData { get; private set; }
        public ScoreModel ScoreModel { get; private set; }

        public StaticData StaticData => _staticData;

        public SceneData SceneData => _sceneData;

        public DeathZone DeathZone => _deathZone;

        public bool Initialized { get; private set; }

        private void Awake()
        {
            if (!GameSceneLoader.EnsureLoaded())
                return;
            _world = new EcsWorld();
            _systems = new EcsSystems(_world);

            Time.timeScale = 0f;

            RuntimeData = new RuntimeData();
            ScoreModel = new ScoreModel();
            ScoreboardService = new PlayFabScoreboardService(FindObjectOfType<PlayfabManager>());
            _systems
                .Add(new HelloWorldSystem(new UnityLogger()))
                .Add(new DtSystem())
                .Add(new TapToPlayInitSystem())
                .Add(new TapToPlaySystem())
                .Add(new PlayerSpawnSystem())
                .Add(new PlayerInvincibilityBonusInitSystem())
                .Add(new SkinSystem())
                .OneFrame<SetSkinCommand>()
                .Add(new CreateLevelGenerationCommandSystem())
                .Add(new GenerateLevelSystem())
                .OneFrame<GenerateLevelCommand>()
                .Add(new CreatePipesSystem())
                .OneFrame<CreatePipeCommand>()
                .Add(new InvincibilityBonusSpawnSystem())
                .OneFrame<CreateInvincibilityBonusCommand>()
                .Add(new PlayerCameraAssignSystem())
                .Add(new UnityGravitySyncSystem())
                .Add(new PlayerJumpInputSystem())
                .Add(new AccelerationSystem())
                .Add(new JumpSystem())
                .Add(new VelocitySystem())
                .Add(new PositionTransformSyncSystem())
                .Add(new PlayerRotationSystem())
                .Add(new DeathZoneSystem())
                .Add(new PlayerDeathSystem())
                .Add(new PlayerDeathCameraSystem())
                .Add(new PlayerRigidbodyDeathSystem())
                .Add(new PublishScoreSystem())
                .Add(new PlayerDeathFinalizeSystem())
                .Add(new BonusInvincibilityTimerSystem())
                .Add(new InvincibilityViewSystem())
                .Add(new IncreaseScoreSystem())
                .OneFrame<IncreaseScoreCommand>()
                .Add(new LoseScreenShowSystem())
                .OneFrame<JumpCommand>()
                .OneFrame<DeathCommand>()
                .OneFrame<PlayerDeathEvent>()
                .OneFrame<TapToPlayEvent>()
                ;

            _systems
                .Inject(_sceneData)
                .Inject(_staticData)
                .Inject(RuntimeData)
                .Inject(new PlayerViewFactory(_staticData, _world))
                .Inject(new UnityControls())
                .Inject(_deathZone)
                .Inject(ScoreboardService)
                .Inject(new PlayerSkinModel(_staticData))
                .Inject(new PlayerSkinView(RuntimeData))
                .Inject(ScoreModel)
                ;
        }

        private void Start()
        {
            _systems.Init();
            Initialized = true;
        }

        private void Update()
        {
            _systems.Run();
        }

        private void OnDestroy()
        {
            if (_systems == null) return;

            _systems.Destroy();
            _systems = null;
            _world.Destroy();
            _world = null;
        }
    }
}