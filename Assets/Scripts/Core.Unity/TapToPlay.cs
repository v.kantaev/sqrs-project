﻿using Core.Movement;
using Leopotam.Ecs;
using UnityEngine;

namespace Core.Unity
{
    public class TapToPlay : MonoBehaviour
    {
        [SerializeField] private GameObject[] _uiToHide;
        private EcsWorld _world;

        public void Init(EcsWorld world)
        {
            _world = world;
        }

        public void Play()
        {
            foreach (var ui in _uiToHide)
            {
                ui.SetActive(false);
            }

            _world.NewEntity().Get<TapToPlayEvent>();
            Time.timeScale = 1f;
        }
    }
}