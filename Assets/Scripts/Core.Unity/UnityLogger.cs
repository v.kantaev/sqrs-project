using UnityEngine;

namespace Core.Unity
{
    public class UnityLogger : ILogger
    {
        public void Log(string message) => Debug.Log(message);
    }
}