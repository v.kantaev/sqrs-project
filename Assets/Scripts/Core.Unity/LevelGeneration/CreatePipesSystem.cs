using Core.LevelGeneration;
using Core.Unity.Numerics;
using Core.Unity.Public.Impl;
using Leopotam.Ecs;
using UnityEngine;

namespace Core.Unity.LevelGeneration
{
    public class CreatePipesSystem : IEcsRunSystem
    {
        private readonly EcsFilter<CreatePipeCommand> _filter = default;
        private readonly StaticData _staticData = default;

        public void Run()
        {
            foreach (var i in _filter)
            {
                var createPipeCommand = _filter.Get1(i);
                var position = createPipeCommand.Position;
                var rotation = createPipeCommand.Rotation;
                Object.Instantiate(_staticData.PipePrefab, position.ToUnityVector(), rotation.ToUnityQuaternion());
            }
        }
    }
}