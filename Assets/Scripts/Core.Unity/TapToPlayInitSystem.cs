using Core.Unity.Public.Impl;
using Leopotam.Ecs;

namespace Core.Unity
{
    public class TapToPlayInitSystem : IEcsInitSystem
    {
        private readonly SceneData _sceneData = default;
        private readonly EcsWorld _world = default;

        public void Init()
        {
            _sceneData.TapToPlay.Init(_world);
        }
    }
}