using Core.Bootstrap;
using Core.Unity.Public.Impl;
using Leopotam.Ecs;
using UnityEngine;

namespace Core.Unity.Bonus
{
    public class PlayerInvincibilityBonusInitSystem : IEcsInitSystem
    {
        private readonly EcsFilter<GameObjectRef, PlayerTag>.Exclude<InvincibilityViewData> _filter = default;

        public void Init()
        {
            foreach (var i in _filter)
            {
                var gameObjectRef = _filter.Get1(i);
                ref var invincibilityViewData = ref _filter.GetEntity(i).Get<InvincibilityViewData>();
                invincibilityViewData.Collider = gameObjectRef.GameObject.GetComponent<Collider>();
                invincibilityViewData.Shield =
                    gameObjectRef.GameObject.transform.Find("Shield").GetComponent<MeshRenderer>();
            }
        }
    }
}