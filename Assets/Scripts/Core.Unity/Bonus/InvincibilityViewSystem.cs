using Core.Bonuses;
using Core.Bootstrap;
using Leopotam.Ecs;

namespace Core.Unity.Bonus
{
    public class InvincibilityViewSystem : IEcsRunSystem
    {
        private readonly EcsFilter<InvincibilityViewData, PlayerTag> _filter = default;

        public void Run()
        {
            foreach (var i in _filter)
            {
                var hasInvincibility = _filter.GetEntity(i).Has<BonusInvincibilityData>();
                var invincibilityViewData = _filter.Get1(i);
                invincibilityViewData.Collider.enabled = !hasInvincibility;
                invincibilityViewData.Shield.enabled = hasInvincibility;
            }
        }
    }
}