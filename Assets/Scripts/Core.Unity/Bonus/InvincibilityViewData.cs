using UnityEngine;

namespace Core.Unity.Bonus
{
    public struct InvincibilityViewData
    {
        public Collider Collider;
        public MeshRenderer Shield;
    }
}