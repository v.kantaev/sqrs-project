using Core.LevelGeneration;
using Core.Unity.Numerics;
using Core.Unity.Public.Impl;
using Leopotam.Ecs;
using UnityEngine;

namespace Core.Unity.Bonus
{
    public class InvincibilityBonusSpawnSystem : IEcsRunSystem
    {
        private readonly EcsFilter<CreateInvincibilityBonusCommand> _filter = default;
        private readonly StaticData _staticData = default;

        public void Run()
        {
            foreach (var i in _filter)
            {
                var position = _filter.Get1(i).Position.ToUnityVector();
                Object.Instantiate(_staticData.InvincibilityBonusPrefab, position, Quaternion.identity);
            }
        }
    }
}