﻿using Core.Bonuses;
using Core.Unity.Public.Impl;
using Leopotam.Ecs;
using UnityEngine;

namespace Core.Unity.Bonus
{
    public class InvincibilityBonus : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            if (!other.TryGetComponent(out EcsEntityBehavior ecsEntityBehavior)) return;
            if (!ecsEntityBehavior.Entity.IsAlive()) return;

            ecsEntityBehavior.Entity.Get<BonusInvincibilityData>();
            Destroy(gameObject);
        }
    }
}