using Core.Public;
using UnityEngine;

namespace Core.Unity.Public.Impl
{
    public class UnityControls : IControls
    {
        public bool JumpPressed => Input.GetButtonDown("Fire1");
    }
}