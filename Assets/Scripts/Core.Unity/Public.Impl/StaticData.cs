using Core.LevelGeneration;
using Core.Public;
using Core.Unity.Scoreboard;
using UnityEngine;

namespace Core.Unity.Public.Impl
{
    [CreateAssetMenu]
    public class StaticData : ScriptableObject, IStaticData
    {
        [SerializeField] private GameObject _playerPrefab;
        [SerializeField] [Min(0f)] private float _playerHorizontalSpeed;
        [SerializeField] [Min(0f)] private float _jumpSpeed;
        [SerializeField] [Min(0f)] private float _gravity;
        [SerializeField] [Min(0f)] private float _invincibilityTime;
        [SerializeField] private AnimationCurve _playerRotationOverVerticalVelocity;
        [SerializeField] [Min(0f)] private float _playerRotationSpeed;
        [SerializeField] private GameObject _pipePrefab;
        [SerializeField] private LevelGenerationParams _levelGenerationParams;
        [SerializeField] private GameObject _invincibilityBonusPrefab;
        [SerializeField] private LoseScreen _loseScreenPrefab;

        public GameObject PlayerPrefab => _playerPrefab;

        public AnimationCurve PlayerRotationOverVerticalVelocity => _playerRotationOverVerticalVelocity;

        public float PlayerRotationSpeed => _playerRotationSpeed;

        public GameObject PipePrefab => _pipePrefab;

        public GameObject InvincibilityBonusPrefab => _invincibilityBonusPrefab;

        public LoseScreen LoseScreenPrefab => _loseScreenPrefab;

        public float PlayerHorizontalSpeed => _playerHorizontalSpeed;

        public float JumpSpeed => _jumpSpeed;

        public float Gravity => _gravity;
        public float InvincibilityTime => _invincibilityTime;

        public LevelGenerationParams LevelGenerationParams => _levelGenerationParams;
    }
}