using Core.Movement;
using Core.Public;
using Core.Unity.Numerics;
using Leopotam.Ecs;
using UnityEngine;
using Quaternion = System.Numerics.Quaternion;
using Vector3 = System.Numerics.Vector3;

namespace Core.Unity.Public.Impl
{
    public class PlayerViewFactory : IPlayerViewFactory
    {
        private readonly StaticData _staticData;
        private readonly EcsWorld _world;

        public PlayerViewFactory(StaticData staticData, EcsWorld world)
        {
            _staticData = staticData;
            _world = world;
        }

        public EcsEntity Create(Vector3 position, Quaternion rotation)
        {
            var entity = _world.NewEntity();
            var gameObject = Object.Instantiate(_staticData.PlayerPrefab,
                position.ToUnityVector(),
                rotation.ToUnityQuaternion()
            );
            entity.Get<GameObjectRef>().GameObject = gameObject;
            entity.Get<PositionData>().Position = position;
            gameObject.AddComponent<EcsEntityBehavior>().Entity = entity;
            return entity;
        }
    }
}