using UnityEngine;

namespace Core.Unity.Public.Impl
{
    public struct GameObjectRef
    {
        public GameObject GameObject;
    }
}