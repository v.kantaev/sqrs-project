using Leopotam.Ecs;
using UnityEngine;

namespace Core.Unity.Public.Impl
{
    public class EcsEntityBehavior : MonoBehaviour
    {
        public EcsEntity Entity { get; set; }
    }
}