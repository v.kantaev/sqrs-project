using Cinemachine;
using Core.Public;
using Core.Skins;
using Core.Unity.Numerics;
using Core.Unity.Skins;
using UnityEngine;
using Vector3 = System.Numerics.Vector3;

namespace Core.Unity.Public.Impl
{
    public class SceneData : MonoBehaviour, ISceneData
    {
        [SerializeField] private Transform _playerSpawnPoint;
        [SerializeField] private CinemachineVirtualCameraBase _playerCamera;
        [SerializeField] private SkinButtonManager _skinButtonManager;
        [SerializeField] private TapToPlay _tapToPlay;
        [SerializeField] private Canvas _canvas;

        public CinemachineVirtualCameraBase PlayerCamera => _playerCamera;

        public TapToPlay TapToPlay => _tapToPlay;

        public Canvas Canvas => _canvas;

        public ISkinButtonManager SkinButtonManager => _skinButtonManager;

        public Vector3 PlayerSpawnPosition => _playerSpawnPoint.position.ToNumericsVector();
    }
}