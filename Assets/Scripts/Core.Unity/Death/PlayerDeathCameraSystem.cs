using Core.Death;
using Core.Unity.Public.Impl;
using Leopotam.Ecs;

namespace Core.Unity.Death
{
    public class PlayerDeathCameraSystem : IEcsRunSystem
    {
        private readonly EcsFilter<PlayerDeathEvent> _filter = default;
        private readonly SceneData _sceneData = default;

        public void Run()
        {
            foreach (var _ in _filter)
            {
                _sceneData.PlayerCamera.Follow = null;
                break;
            }
        }
    }
}