using Core.Death;
using Core.Unity.Public.Impl;
using Leopotam.Ecs;
using UnityEngine;

namespace Core.Unity.Death
{
    public class DeathZone : MonoBehaviour, IDeathZone
    {
        private void OnTriggerEnter(Collider other)
        {
            if (!other.TryGetComponent(out EcsEntityBehavior ecsEntityBehavior)) return;
            if (!ecsEntityBehavior.Entity.IsAlive()) return;

            ecsEntityBehavior.Entity.Get<DeathCommand>();
        }

        public void SetZ(float z)
        {
            var myTransform = transform;
            var position = myTransform.position;
            position.z = z;
            myTransform.position = position;
        }
    }
}