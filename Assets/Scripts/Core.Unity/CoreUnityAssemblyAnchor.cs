using JetBrains.Annotations;

namespace Core.Unity
{
    [UsedImplicitly]
    public class CoreUnityAssemblyAnchor { }
}