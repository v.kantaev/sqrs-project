using Core.Public;
using Leopotam.Ecs;
using UnityEngine;

namespace Core.Unity.TimeManagement
{
    public class DtSystem : IEcsRunSystem
    {
        private readonly RuntimeData _data = default;

        public void Run()
        {
            _data.Dt = Time.deltaTime;
        }
    }
}