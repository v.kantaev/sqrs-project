using Core.Public;
using Leopotam.Ecs;
using UnityEngine;

namespace Core.Unity.Movement
{
    public class UnityGravitySyncSystem : IEcsInitSystem
    {
        private readonly IStaticData _staticData = default;

        public void Init()
        {
            Physics.gravity = Vector3.down * _staticData.Gravity;
        }
    }
}