using Core.Bootstrap;
using Core.Movement;
using Core.Public;
using Core.Unity.Public.Impl;
using Leopotam.Ecs;
using UnityEngine;

namespace Core.Unity.Movement
{
    public class PlayerRotationSystem : IEcsRunSystem
    {
        private readonly EcsFilter<VelocityData, GameObjectRef, PlayerTag> _filter = default;
        private readonly RuntimeData _runtimeData = default;
        private readonly StaticData _staticData = default;

        public void Run()
        {
            foreach (var i in _filter)
            {
                var velocity = _filter.Get1(i).Velocity;
                var transform = _filter.Get2(i).GameObject.transform;
                var targetXAngle = _staticData.PlayerRotationOverVerticalVelocity.Evaluate(velocity.Y);
                var currentXAngle = transform.eulerAngles.x;
                var xAngle = Mathf.MoveTowardsAngle(currentXAngle, targetXAngle,
                    _staticData.PlayerRotationSpeed * _runtimeData.Dt
                );
                transform.eulerAngles = new Vector3(xAngle, 0f, 0f);
            }
        }
    }
}