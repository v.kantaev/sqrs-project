using Core.Bootstrap;
using Core.Death;
using Core.Movement;
using Core.Unity.Numerics;
using Core.Unity.Public.Impl;
using Leopotam.Ecs;
using UnityEngine;

namespace Core.Unity.Movement
{
    public class PlayerRigidbodyDeathSystem : IEcsRunSystem
    {
        private readonly EcsFilter<PlayerDeathEvent> _deathEvents = default;
        private readonly EcsFilter<VelocityData, GameObjectRef, PlayerTag> _players = default;

        public void Run()
        {
            foreach (var _ in _deathEvents)
            {
                foreach (var i in _players)
                {
                    var rigidbody = _players.Get2(i).GameObject.GetComponent<Rigidbody>();
                    rigidbody.isKinematic = false;
                    rigidbody.velocity = _players.Get1(i).Velocity.ToUnityVector();
                }

                break;
            }
        }
    }
}