using Core.Movement;
using Core.Unity.Numerics;
using Core.Unity.Public.Impl;
using Leopotam.Ecs;

namespace Core.Unity.Movement
{
    public class PositionTransformSyncSystem : IEcsRunSystem
    {
        private readonly EcsFilter<GameObjectRef, PositionData> _filter = default;

        public void Run()
        {
            foreach (var i in _filter)
            {
                var gameObject = _filter.Get1(i).GameObject;
                var transform = gameObject.transform;
                transform.position = _filter.Get2(i).Position.ToUnityVector();
            }
        }
    }
}